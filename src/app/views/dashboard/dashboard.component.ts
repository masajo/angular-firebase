import { Component, OnInit } from '@angular/core';
import { FireStoreAuthService } from 'src/app/shared/services/auth/fire-store-auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(
    public authService: FireStoreAuthService
  ) { }

  ngOnInit(): void {
  }

}
