import { Injectable } from '@angular/core';
// Angular Firestore nos va a permitir acceder a las colecciones
// y a toda la API de FireStore para sentencias CRUD (Create, Read, Update y Delete)
import { AngularFirestore, AngularFirestoreCollection, DocumentReference } from '@angular/fire/firestore';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// Imports necesarios
import { Comunidad } from '../models/comunidad.interface';
import { Receta } from '../models/receta.interface';

@Injectable({
  providedIn: 'root'
})
export class FireStoreService {

  // Colección de Comunidades y Recetas:
  private comunidadesCollection: AngularFirestoreCollection<Comunidad>;
  private recetasCollection: AngularFirestoreCollection<Receta>;

  // Listas de Comunidades y Recetas
  private listaComunidades: Observable<Comunidad[]>;
  private listaRecetas: Observable<Receta[]>;



  constructor(private fireStore: AngularFirestore) {

    // Colecciones de FireStore referenciadas por el nombre y tipo de datos

    // * Comunidades
    this.comunidadesCollection = this.fireStore.collection<Comunidad>('comunidades');
    this.listaComunidades = this.comunidadesCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(action => {
          const datos = action.payload.doc.data();
          const id = action.payload.doc.id;
          // Devolvemos la comunidad junto con su ID de FireStore
          return {
            id,
            ...datos
          }
        });
      })
    );

    // * Recetas
    this.recetasCollection = this.fireStore.collection<Receta>('recetas');
    this.listaRecetas = this.recetasCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(action => {
          const datos = action.payload.doc.data();
          const id = action.payload.doc.id;
          // Devolvemos la receta junto con su ID de FireStore
          return {
            id,
            ...datos
          }
        });
      })
    );
  }


  // ******** COMUNIDADES ***********

  /**
   * (GET ALL)
   * Método que se conecta con la colección "comunidades"
   * y devuelve la lista de las mismas
   */
  obtenerTodasLasComunidades(): Observable<Comunidad[]>{
    return this.listaComunidades;
  }

  /**
   * Método para obtener una comunidad por ID
   * @param id ID de la comunidad
   */
  obtenerComunidad(id: string): Observable<Comunidad | undefined>{
    return this.comunidadesCollection.doc<Comunidad>(id).valueChanges();
  }

  /**
   * @param comuniad Comunidad a crear
   * @returns Una referencia a la comunidad recién creada
   */
  crearComunidad(comunidad: Comunidad): Promise<DocumentReference<Comunidad>> {
    return this.comunidadesCollection.add(comunidad);
  }

  /**
   *
   * @param comunidadActualizada Nuevos datos para la comunidad
   * @param id de la comunidad que se quiere modificar
   * @returns Devuelve nada
   */
  actualizarComunidad(comunidadActualizada: Comunidad, id: string): Promise<void> {
    return this.comunidadesCollection.doc<Comunidad>(id).update(comunidadActualizada);
  }

  /**
   *
   * @param id De la comunidad que se quiere eliminar
   * @returns nada
   */
  borrarComunidad(id: string): Promise<void>{
    return this.comunidadesCollection.doc<Comunidad>(id).delete();
  }



  // ******** RECETAS ***********
  /**
   * Método que se conecta con la colección "recetas"
   * y devuelve la lista de las mismas
   */
   obtenerTodasLasRecetas(): Observable<Receta[]>{
     return this.listaRecetas;
  }


}
