import { Injectable, NgZone } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import firebase from 'firebase';

import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { User } from '../../models/user.interface';


@Injectable({
  providedIn: 'root'
})
export class FireStoreAuthService {

  // Datos del usuario
  userData: any;

  constructor(
    private afs: AngularFirestore,
    private afAuth: AngularFireAuth,
    private router: Router,
    private ngZone: NgZone
  ) {

    // Guardamos los datos del usuario en localStorage
    // del navegador
    this.afAuth.authState.subscribe(user => {
      // Si hay usuario autenticado...
      if (user) {
        // lo guardamos en localStorage bajo la clave user
        localStorage.setItem('user', JSON.stringify(this.userData));
      } else {
        // Si no, Eliminamos los datos del usuario
        localStorage.setItem('user', '');
      }
    });
  }

  /**
   * Método que sirve para verificar que el usuario está logueado
   * y que puede acceder a las rutas controladas por el Guard
   */
  get isLoggedIn(): boolean {
    const user:User = JSON.parse(localStorage.getItem('user') || '');
    return (user != null && user.registered !== false) ? true : false;
  }



  /**
   * Método para realizar un Login en FirebaseAuth
   * @param email
   * @param password
   */
  login(email: string, password: string) {
    return this.afAuth.signInWithEmailAndPassword(email, password)
      .then((result: any) => {
      this.ngZone.run(() => {
        // Navegamos a la ruta HOME
        this.router.navigate(['home']);
      });
      this.SetUserData(result.user);
    }).catch((err) => {
      window.alert(err.message);
    })
  }

  /**
   * Método para registrar usuarios en Firebase
   * @param email Email del usuario
   * @param password Contraseña
   * @returns
   */
  register(email: string, password: string) {
    return this.afAuth.createUserWithEmailAndPassword(email, password)
      .then((result:any) => {
        /* Call the SendVerificaitonMail() function when new user sign
        up and returns promise */
        this.SendVerificationMail();
        this.SetUserData(result.user);
      }).catch((error) => {
        window.alert(error.message)
      })
  }

  // Enviamos un email de verficación al usuario que se registra
  async SendVerificationMail() {
    return this.afAuth.currentUser.then(user => user?.sendEmailVerification()).then(() => {
      this.router.navigate(['verify-email']);
    });
  }

  // Reset Forggot password
  ForgotPassword(passwordResetEmail: string) {
    return this.afAuth.sendPasswordResetEmail(passwordResetEmail)
    .then(() => {
      window.alert('Te hemos enviado un email. Comprueba tu bandeja de entrada.');
    }).catch((error) => {
      window.alert(error)
    })
  }

  // Login con Google Provider
  GoogleAuth() {
    return this.AuthLogin(new firebase.auth.GoogleAuthProvider());
  }

  /**
   * Método para realizar Login a través de
   * diferentes proveedores
   */
  AuthLogin(provider: any) {
    return this.afAuth.signInWithPopup(provider)
    .then((result: any) => {
       this.ngZone.run(() => {
          this.router.navigate(['dashboard']);
        })
      this.SetUserData(result.user);
    }).catch((error) => {
      window.alert(error)
    })
  }


  /* Setting up user data when sign in with username/password,
  sign up with username/password and sign in with social auth
  provider in Firestore database using AngularFirestore + AngularFirestoreDocument service */
  SetUserData(user: User) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    const userData: User = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      registered: user.registered
    }
    return userRef.set(userData, {
      merge: true
    })
  }

  // Logout de firebase
  logout() {
    return this.afAuth.signOut().then(() => {
      localStorage.removeItem('user');
      this.router.navigate(['login']);
    })
  }
}
