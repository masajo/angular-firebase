import { TestBed } from '@angular/core/testing';

import { FireStoreAuthService } from './fire-store-auth.service';

describe('FireStoreAuthService', () => {
  let service: FireStoreAuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FireStoreAuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
