export interface Receta {
  nombre: string,
  descripcion: string,
  imagen: string,
  ingredientes: string[],
  tiempo: number,
  comunidad: string,
}
