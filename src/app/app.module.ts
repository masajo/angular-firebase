import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

// Firebase Modules
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';

// Variables de entorno
import { environment } from '../environments/environment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { ComunidadesPageComponent } from './pages/comunidades-page/comunidades-page.component';
import { RecetasPageComponent } from './pages/recetas-page/recetas-page.component';
import { NavComponent } from './views/nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { RegisterPageComponent } from './pages/register-page/register-page.component';
import { ForgotPasswordPageComponent } from './pages/forgot-password-page/forgot-password-page.component';
import { VerifyEmailPageComponent } from './pages/verify-email-page/verify-email-page.component';
import { NotFoundPageComponent } from './pages/not-found-page/not-found-page.component';
import { FireStoreAuthService } from './shared/services/auth/fire-store-auth.service';
import { FireStoreService } from './shared/services/fire-store.service';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { DashboardPageComponent } from './pages/dashboard-page/dashboard-page.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ComunidadesPageComponent,
    RecetasPageComponent,
    NavComponent,
    LoginPageComponent,
    RegisterPageComponent,
    ForgotPasswordPageComponent,
    VerifyEmailPageComponent,
    NotFoundPageComponent,
    DashboardComponent,
    DashboardPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    // Añadimos los módulos de Firebase y FireStore
    // y lo configuramos con los datos de la aplicación en
    // la variable de entorno firebaseConfig
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
  ],
  providers: [FireStoreAuthService, FireStoreService],
  bootstrap: [AppComponent]
})
export class AppModule { }
