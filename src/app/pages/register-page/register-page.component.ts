import { Component, OnInit } from '@angular/core';
import { FireStoreAuthService } from 'src/app/shared/services/auth/fire-store-auth.service';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.scss']
})
export class RegisterPageComponent implements OnInit {

  constructor(
    public authService: FireStoreAuthService
  ) { }

  ngOnInit(): void {
  }

}
