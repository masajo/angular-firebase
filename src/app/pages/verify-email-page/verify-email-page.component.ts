import { Component, OnInit } from '@angular/core';
import { FireStoreAuthService } from 'src/app/shared/services/auth/fire-store-auth.service';

@Component({
  selector: 'app-verify-email-page',
  templateUrl: './verify-email-page.component.html',
  styleUrls: ['./verify-email-page.component.scss']
})
export class VerifyEmailPageComponent implements OnInit {

  constructor(
    public authService: FireStoreAuthService
  ) { }

  ngOnInit(): void {
  }

}
