import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComunidadesPageComponent } from './comunidades-page.component';

describe('ComunidadesPageComponent', () => {
  let component: ComunidadesPageComponent;
  let fixture: ComponentFixture<ComunidadesPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComunidadesPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComunidadesPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
