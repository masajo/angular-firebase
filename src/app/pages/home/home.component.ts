import { Component, OnInit } from '@angular/core';
import { Comunidad } from 'src/app/shared/models/comunidad.interface';
import { Receta } from 'src/app/shared/models/receta.interface';
import { FireStoreService } from 'src/app/shared/services/fire-store.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  // Listados de las colecciones de Firebase (Comunidades y Recetas)
  listadoComunidades: Comunidad[] = [];
  listadoRecetas: Receta[] = [];


  constructor(private firestoreService: FireStoreService) { }

  ngOnInit(): void {

    this.firestoreService.obtenerTodasLasComunidades()
      .subscribe((comunidades: Comunidad[]) => {
        this.listadoComunidades = comunidades;
        console.table(this.listadoComunidades); // comprobamos que nos han venido bien
      });

    this.firestoreService.obtenerTodasLasRecetas()
      .subscribe((recetas: Receta[]) => {
        this.listadoRecetas = recetas;
        console.table(this.listadoRecetas); // comprobamos que nos han venido bien
      });
  }

  crearComunidad() {
    let nuevaComunidad: Comunidad = {
      nombre: "Prueba",
    }

    this.firestoreService.crearComunidad(nuevaComunidad).then((response) => {
      console.log('Comunidad creada con éxito');
      console.table(response)
    }).catch((err) => {
      console.log(`Error al crear la comunidad: ${err}`);
    });
  }

  borrarComunidad(id:string) {
    this.firestoreService.borrarComunidad(id).then(_ => {
      console.log(`Comunidad con id ${id} borrada con éxito`)
    }).catch((err) => {
      console.log(`Error al borrar la comunidad con id: ${id}: ${err}`)
    });
  }


}
