import { Component, OnInit } from '@angular/core';
import { FireStoreAuthService } from 'src/app/shared/services/auth/fire-store-auth.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  constructor(public authService: FireStoreAuthService) { }

  ngOnInit(): void {
  }

}
