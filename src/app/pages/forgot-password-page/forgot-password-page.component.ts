import { Component, OnInit } from '@angular/core';
import { FireStoreAuthService } from 'src/app/shared/services/auth/fire-store-auth.service';

@Component({
  selector: 'app-forgot-password-page',
  templateUrl: './forgot-password-page.component.html',
  styleUrls: ['./forgot-password-page.component.scss']
})
export class ForgotPasswordPageComponent implements OnInit {

  constructor(
    public authService: FireStoreAuthService
  ) { }

  ngOnInit(): void {
  }

}
