// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyA31pFpVi0ToYaG6YvJbUJxeC9CtxzrB_M",
    authDomain: "gastroapp-1a7f7.firebaseapp.com",
    projectId: "gastroapp-1a7f7",
    storageBucket: "gastroapp-1a7f7.appspot.com",
    messagingSenderId: "575316236487",
    appId: "1:575316236487:web:3a01d0390a1387cbdac6e2",
    measurementId: "G-63BZ8D03QP"
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
